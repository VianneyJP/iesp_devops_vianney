terraform {
  backend "gcs" {
    bucket      = "storageiespvianney"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}